<?php

namespace Metinet\Http;

/**
 * @author Boris Guéry <guery.b@gmail.com>
 */
class Response
{
    private $statusCode;
    private $content;
    private $headers = array();

    public function __construct($content, $statusCode = 200,
                                array $headers = array())
    {
        $this->statusCode = (int) $statusCode;
        $this->content    = $content;
        $this->headers    = $headers;
    }

    public function send()
    {
//        http_response_code($this->statusCode);
        foreach ($this->headers as $headerName => $headerValue) {
            header(sprintf("%s: %s", $headerName, $headerValue));
        }
        echo $this->content;
    }
}
