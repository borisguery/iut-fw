<?php

namespace Metinet\Http;

/**
 * @author Boris Guéry <guery.b@gmail.com>
 */
class Request
{
    private $method;
    private $body;
    private $uri;
    private $headers;
    private $query;

    public function __construct($method, $uri, array $headers = array(),
                                array $query = array(), $body = null)
    {
        $this->method  = $method;
        $this->uri     = $uri;
        $this->query   = $query;
        $this->headers = $headers;
        $this->body    = $body;
    }

    static public function createFromGlobals()
    {
        $headers = array();
        foreach ($_SERVER as $key => $value) {
            if (0 === strpos($key, 'HTTP_')) {
                $normalizedHeaderName = substr($key, 5);
                $normalizedHeaderName = str_replace(
                    '_',
                    '-',
                    $normalizedHeaderName
                );
                $normalizedHeaderName = strtolower($normalizedHeaderName);
                $headers[$normalizedHeaderName] = $value;
            }
        }

        if (isset($_SERVER['CONTENT_TYPE'])) {
            $headers['content-type'] = $_SERVER['CONTENT_TYPE'];
        }

        $body = file_get_contents('php://input');

        $method = $_SERVER['REQUEST_METHOD'];

        $uriSegments = explode("?", $_SERVER['REQUEST_URI']);
        $uri = $uriSegments[0];

        $rawQuery = isset($_SERVER['QUERY_STRING'])
            ? $_SERVER['QUERY_STRING']
            : '';

        parse_str($rawQuery, $query);

        $request = new self(
            $method,
            $uri,
            $headers,
            $query,
            $body
        );

        return $request;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function getFormData()
    {
        if (preg_match("#application/x-www-form-urlencoded#", $this->headers["content-type"])) {
            parse_str($this->body, $data);

            return $data;
        }

        return $this->body;
    }

    public function getUri()
    {
        return $this->uri;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    public function getQuery()
    {
        return $this->query;
    }

    public function isMethod($method)
    {
        return (strtolower($this->method) === strtolower($method));
    }

    public function isPost()
    {
        return $this->isMethod("post");
    }
}

















