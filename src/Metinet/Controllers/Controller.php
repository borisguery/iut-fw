<?php

namespace Metinet\Controllers;
use Metinet\Http\Request;
use Metinet\Http\Response;
use Metinet\Repository\TodoRepository;
use Metinet\View\PhpViewRenderer;
use Metinet\View\ViewRenderer;

/**
 * @author Boris Guéry <guery.b@gmail.com>
 */
class Controller
{
    private $todoRepository;
    private $viewRenderer;

    public function __construct(TodoRepository $todoRepository, ViewRenderer $viewRenderer)
    {
        $this->viewRenderer = $viewRenderer;
        $this->todoRepository = $todoRepository;
    }

    public function homeAction(Request $request)
    {
        $query = $request->getQuery();
        $name = isset($query["name"]) ? $query["name"] : "inconnu";



        $todos = $this->todoRepository->getTodos();

        return new Response($this->viewRenderer->render("home.phtml", array("todos" => $todos)));
    }

    public function aboutAction()
    {
        $message = "About";

        return new Response($message);
    }

    public function addTodoProcessAction(Request $request)
    {

        var_dump($request->getFormData());
        return new Response("Terms of service");
    }
}
