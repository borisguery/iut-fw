<?php
/**
 * @author Boris Guéry <guery.b@gmail.com>
 */

namespace Metinet\Controllers;

use Metinet\Http\Response;
use Metinet\View\ViewRenderer;

class ErrorController
{
    private $viewRenderer;
    /**
     * @var \Exception
     */
    private $exception;

    public function __construct(ViewRenderer $viewRenderer)
    {
        $this->viewRenderer = $viewRenderer;
    }

    public function setException(\Exception $exception)
    {
        $this->exception = $exception;
    }

    public function genericErrorAction()
    {
        $parameters = array();
        $parameters["message"] = isset($this->exception)
            ? $this->exception->getMessage()
            : "Unable to get exception"
        ;

        return new Response($this->viewRenderer->render("genericError.phtml", $parameters));
    }
}
