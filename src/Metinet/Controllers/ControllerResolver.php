<?php
/**
 * @author Boris Guéry <guery.b@gmail.com>
 */

namespace Metinet\Controllers;

use Metinet\Http\Request;
use Metinet\Routing\RouteMatcher;

class ControllerResolver
{
    private $matcher;
    private $controllers = array();

    public function __construct(RouteMatcher $matcher)
    {
        $this->matcher = $matcher;
    }

    public function resolve(Request $request)
    {
        $action = $this->matcher->match($request);
        $splitAction = explode('::', $action);
        $className = "\\" . ltrim($splitAction[0], "\\");
        foreach ($this->controllers as $controller) {
            if ($className === '\\' . get_class($controller)) {
                $action = $splitAction[1] . "Action";

                return array($controller, $action);
            }
        }

        throw new ControllerNotFound($className);
    }

    public function addController($controllerInstance)
    {
        if (!is_object($controllerInstance)) {
            throw new \Exception('$controllerInstance must be an object');
        }

        $this->controllers[] = $controllerInstance;
    }
}
















