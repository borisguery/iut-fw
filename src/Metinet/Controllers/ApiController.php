<?php
/**
 * @author Boris Guéry <guery.b@gmail.com>
 */

namespace Metinet\Controllers;

use Metinet\Http\Response;

class ApiController
{
    public function __construct()
    {
    }

    public function tosAction()
    {
        return new Response(
            json_encode(
                array("Message en JSON")
            )
        );
    }
}
