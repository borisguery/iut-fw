<?php
/**
 * @author Boris Guéry <guery.b@gmail.com>
 */

namespace Metinet\Configuration;


class PhpLoader implements Loader
{
    private $filenames;

    public function __construct(array $filenames)
    {
        $this->filenames = $filenames;
    }

    public function load()
    {
        $config = array();
        foreach ($this->filenames as $filename) {
            $config = array_merge_recursive($config, include $filename);
        }

        return $config;
    }
}
