<?php
/**
 * @author Boris Guéry <guery.b@gmail.com>
 */

namespace Metinet\Configuration;

interface Loader
{
    /**
     * @return array
     */
    public function load();
}
