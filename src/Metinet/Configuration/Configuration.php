<?php

namespace Metinet\Configuration;

/**
 * @author Boris Guéry <guery.b@gmail.com>
 */
class Configuration
{
    private $config = array();
    private $loader;

    public function __construct(Loader $loader)
    {
        $this->loader = $loader;
    }

    public function get($name, $default = null)
    {
        $this->load();

        $segments = explode("/", $name);

        $val = $default;
        if (isset($this->config[$segments[0]])) {
            $val = $this->config[$segments[0]];
            array_shift($segments);
            foreach ($segments as $segment) {
                $val = $val[$segment];
            }
        }

        return $val;
    }

    public function all()
    {
        $this->load();

        return $this->config;
    }

    private function load()
    {
        if (count($this->config) <= 0) {
            $this->config = $this->loader->load();
        }
    }
}
