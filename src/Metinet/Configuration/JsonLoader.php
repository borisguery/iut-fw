<?php
/**
 * @author Boris Guéry <guery.b@gmail.com>
 */

namespace Metinet\Configuration;

class JsonLoader implements Loader
{
    private $filenames;

    public function __construct(array $filenames)
    {
        $this->filenames = $filenames;
    }

    public function load()
    {
        $config = array();
        foreach ($this->filenames as $filename) {
            $config = array_merge_recursive(
                $config,
                json_decode(file_get_contents($filename), true)
            );
        }

        return $config;
    }
}
