<?php
/**
 * @author Boris Guéry <guery.b@gmail.com>
 */

namespace Metinet\Configuration;


class ChainLoader implements Loader
{
    /**
     * @var Loader[]
     */
    private $loaders = array();

    public function __construct($loaders = array())
    {
        foreach ($loaders as $loader) {
            if (!$loader instanceof Loader) {
                throw new \InvalidArgumentException(sprintf("Invalid loader provided (type: %s)", gettype($loader)));
            }
        }

        $this->loaders = $loaders;
    }

    public function load()
    {
        $config = array();

        foreach ($this->loaders as $loader) {
            $config += $loader->load();
        }

        return $config;
    }
}













