<?php

namespace Metinet\View;

class PhpViewRenderer implements ViewRenderer
{
    private $viewsDirectory;
    private $layout;

    public function  __construct($layout, $viewsDirectory)
    {
        $this->viewsDirectory = $viewsDirectory;
        $this->layout         = $layout;
    }

    public function render($viewName, array $parameters = array())
    {
        ob_start();
        include $this->viewsDirectory . $viewName;
        $_content = ob_get_contents();
        ob_clean();

        ob_start();
        include $this->layout;
        $view = ob_get_contents();
        ob_clean();

        return $view;
    }
}
