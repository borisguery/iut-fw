<?php

namespace Metinet\View;

interface ViewRenderer
{
    public function render($viewName, array $parameters = array());
}
