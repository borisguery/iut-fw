<?php
/**
 * @author Boris Guéry <guery.b@gmail.com>
 */

namespace Metinet\Logger;

interface Logger
{
    public function log($message);
}
