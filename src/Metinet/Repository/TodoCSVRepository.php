<?php
namespace Metinet\Repository;
/**
 * @author Boris Guéry <guery.b@gmail.com>
 */
class TodoCSVRepository implements TodoRepository
{
    private $todoFile;

    public function __construct($todoFile)
    {
        $this->todoFile = $todoFile;
    }

    public function getTodos()
    {
        $todos = array();
        $handle = fopen($this->todoFile, "r");
        while (false !== ($data = fgetcsv($handle, 1024, ","))) {
            $todos[] = array("id" => $data[0], "text" => $data[1]);
        }
        fclose($handle);

        return $todos;

    }
}
