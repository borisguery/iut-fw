<?php
namespace Metinet\Repository;
/**
 * @author Boris Guéry <guery.b@gmail.com>
 */
class TodoPDORepository implements TodoRepository
{
    private $pdo;

    public function __construct($host, $databaseName, $user, $password)
    {
        $dsn = sprintf("mysql:host=%s;dbname=%s", $host, $databaseName);
        $this->pdo = new \PDO($dsn, $user, $password);
    }

    public function getTodos()
    {
        $statement = $this->pdo->query("SELECT * FROM todos");
        $rows = $statement->fetchAll(\PDO::FETCH_ASSOC);

        return $rows;
    }
}
