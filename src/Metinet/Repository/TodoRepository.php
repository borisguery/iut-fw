<?php
/**
 * @author Boris Guéry <guery.b@gmail.com>
 */

namespace Metinet\Repository;

interface TodoRepository
{
    public function getTodos();
}
