<?php
/**
 * @author Boris Guéry <guery.b@gmail.com>
 */

namespace Metinet\Domain;

class ConferenceDate
{

    private $date;

    public function __construct(\DateTime $date)
    {
        Assert::dateNotBefore(
            new \DateTime("now"),
            $date
        );
        $this->date = $date;
    }

    public function format($format)
    {
        return $this->date->format($format);
    }

    public function __toString()
    {
        return $this->format(\DateTime::ISO8601);
    }
}
