<?php
/**
 * @author Boris Guéry <guery.b@gmail.com>
 */

namespace Metinet\Domain;


class UnavailableLocation extends \InvalidArgumentException
{
    public function __construct($city, $availableLocations = array())
    {
        parent::__construct(
            sprintf(
                "%s is not avaible (available locations are: %s",
                $city, rtrim(implode(", ", $availableLocations), ",")
                )
        );
    }
}
