<?php

namespace Metinet\Domain;

use InvalidArgumentException;

/**
 * @author Boris Guéry <guery.b@gmail.com>
 */
class Conference
{

    private $title;
    private $date;
    private $location;
    private $isPublished;
    private $attendees = array();

    public function __construct(Title $title, ConferenceDate $date, Location $location, $isPublished = false)
    {
        $this->title = $title;
        $this->date  = $date;

        $this->validateLocation($location);

        $this->location = $location;
        $this->isPublished = $isPublished;
    }

    private function validateLocation(Location $location)
    {
        if (!AvailableLocations::exists($location)) {
            throw new UnavailableLocation(
                $location->getCity(),
                array("Paris", "Aix", "Bourg")
            );
        }
    }
}
