<?php
/**
 * @author Boris Guéry <guery.b@gmail.com>
 */

namespace Metinet\Domain;

use InvalidArgumentException;

class Title
{
    private $title;

    public function __construct($title)
    {
        $this->validateTitle($title);

        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function __toString()
    {
        return $this->getTitle();
    }

    private function validateTitle($title)
    {
        Assert::notEmpty($title, "Title must not be empty");
        Assert::maxLength($title, 255);
    }
}
