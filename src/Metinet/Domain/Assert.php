<?php
/**
 * @author Boris Guéry <guery.b@gmail.com>
 */

namespace Metinet\Domain;

class Assert
{
    static public function notEmpty($value, $message = "Value must not be empty")
    {
        if (empty($value)) {
            throw new \InvalidArgumentException($message);
        }
    }

    static public function maxLength($value, $length, $message = "Max length reached")
    {
        if (strlen($value) > $length) {
            throw new \InvalidArgumentException($message);
        }
    }

    static public function dateNotBefore(\DateTime $beforeDate, \DateTime $date)
    {
        return true;
    }

    static public function integer($value, $message = "Must an integer")
    {
        if (!is_int($value)) {
            throw new \InvalidArgumentException($message);
        }
    }

    static public function numeric($value, $message = "Must an integer")
    {
        if (!is_numeric($value)) {
            throw new \InvalidArgumentException($message);
        }
    }

    static public function boolean($value, $message)
    {
        if (!is_bool($value)) {
            throw new \InvalidArgumentException($message);
        }
    }
}








