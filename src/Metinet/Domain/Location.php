<?php
/**
 * @author Boris Guéry <guery.b@gmail.com>
 */

namespace Metinet\Domain;

class Location
{
    private $city;
    private $maxAttendees;

    public function __construct($city, $maxAttendees = "a")
    {
        Assert::integer($maxAttendees);
        Assert::notEmpty($city);

        $this->city = $city;
        $this->maxAttendees = $maxAttendees;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function equals(Location $location)
    {
        return ($location->city === $this->city && $location->maxAttendees === $this->maxAttendees);
    }
}
