<?php
/**
 * @author Boris Guéry <guery.b@gmail.com>
 */

namespace Metinet\Domain;

class AvailableLocations
{
    private static $locations = array(
        array("city" => "Paris", "maxAttendees" => 1000),
        array("city" => "Aix en Provence", "maxAttendees" => 100),
        array("city" => "Lille", "maxAttendees" => 50),
        array("city" => "Bourg en Bresse", "maxAttendees" => 22),
    );

    static public function get($city) {
        Assert::notEmpty($city);
        if (!isset(self::$locations[$city])) {
            throw new UnknownLocation($city);
        }

        return self::$locations[$city];
    }

    static public function exists(Location $location)
    {
        foreach (self::$locations as $loc) {
            if ($location->equals(new Location($loc["city"], $loc["maxAttendees"]))) {
                return true;
            }
        }

        return false;
    }
}
