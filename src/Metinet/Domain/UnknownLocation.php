<?php
/**
 * @author Boris Guéry <guery.b@gmail.com>
 */

namespace Metinet\Domain;

class UnknownLocation extends \InvalidArgumentException
{
    public function __construct($city)
    {
        parent::__construct(sprintf("%s doesn't exist", $city));
    }
}
