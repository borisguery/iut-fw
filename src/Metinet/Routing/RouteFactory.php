<?php
/**
 * @author Boris Guéry <guery.b@gmail.com>
 */

namespace Metinet\Routing;

use Metinet\Configuration\Configuration;

class RouteFactory
{
    /**
     * @param Configuration $configuration
     * @return Route[]
     */
    static public function fromConfiguration(Configuration $configuration)
    {
        $routes = array();
        foreach ($configuration->get("routes") as $route) {
            $routes[] = Route::fromArray($route);
        }

        return $routes;
    }
}
