<?php
/**
 * @author Boris Guéry <guery.b@gmail.com>
 */

namespace Metinet\Routing;

use Metinet\Http\Request;

class RouteMatcher
{
    /** @var Route[] */
    private $routes = array();

    public function __construct(array $routes)
    {
        foreach ($routes as $route) {
            if (!$route instanceof Route) {
                throw new \InvalidArgumentException("Must be a Route object");
            }
        }
        $this->routes = $routes;
    }

    public function match(Request $request)
    {
        foreach ($this->routes as $route) {
            if ($route->getUrl() === $request->getUri()
                && $request->isMethod($route->getMethod())
            ) {
                return $route->getAction();
            }
        }

        throw new RouteNotFound($request);

    }
}
