<?php

return array(
    "todosStorageType" => "mysql",
    "todosFile" => __DIR__ . "/../data/todos.csv",
    "todosPerPage" => 10,
    "database" => [
        "host" => "172.16.1.127",
        "database" => "iut",
        "user" => "root",
        "password" => ""
    ]
);
