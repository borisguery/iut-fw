<?php

require __DIR__ . "/../vendor/autoload.php";

use Metinet\Domain\Conference;
use Metinet\Domain\Title;
use Metinet\Domain\ConferenceDate;
use Metinet\Domain\AvailableLocations;

$title = new Title("Conférence METINET");
$date = new ConferenceDate(new \DateTime("next month"));
$conference = new Conference(
    $title,
    $date,
    new \Metinet\Domain\Location("Paris", 1000),
    true
);

var_dump($conference);
