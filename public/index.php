<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('html_errors', 1);

require __DIR__ . "/../vendor/autoload.php";

use Metinet\Http\Response;
use Metinet\Http\Request;
use Metinet\Configuration\Configuration;
use Metinet\Configuration\PhpLoader;
use Metinet\Configuration\JsonLoader;
use Metinet\Routing\RouteMatcher;
use Metinet\Routing\RouteFactory;
use Metinet\Controllers\ControllerResolver;
use Metinet\Controllers\ApiController;
use Metinet\Controllers\Controller;
use Metinet\Configuration\ChainLoader;
use Metinet\View\PhpViewRenderer;

$chainLoader = new ChainLoader(
    array(
        new PhpLoader(array(__DIR__ . "/../config/app.dev.php")),
        new JsonLoader(
            array(
                __DIR__ . "/../config/app.global.json",
                __DIR__ . "/../config/routes.json",

            )
        )
    )
);

$configuration = new Configuration($chainLoader);

$routes = RouteFactory::fromConfiguration($configuration);

$routeMatcher = new RouteMatcher($routes);

$controllerResolver = new ControllerResolver($routeMatcher);

if ("csv" === $configuration->get("todosStorageType")) {
    $todoRepo = new \Metinet\Repository\TodoCSVRepository($configuration->get("todosFile"));
} elseif ("mysql" === $configuration->get("todosStorageType")) {
    $todoRepo = new \Metinet\Repository\TodoPDORepository(
        $configuration->get("database/host"),
        $configuration->get("database/database"),
        $configuration->get("database/user"),
        $configuration->get("database/password")
    );
} else {
    throw new InvalidArgumentException(sprintf("Invalid Storage type provided: %s", $configuration->get("todosStorageType")));
}

$viewRenderer = new PhpViewRenderer(
    __DIR__ . "/../src/Metinet/Resources/views/layout.phtml",
    __DIR__ . "/../src/Metinet/Resources/views/"
);

$controllerResolver->addController(new Controller($todoRepo, $viewRenderer));
$controllerResolver->addController(new ApiController());

$callArgs = array();

try {
    $request = Request::createFromGlobals();
    $resolvedController = $controllerResolver->resolve($request);

    $r = new ReflectionMethod($resolvedController[0], $resolvedController[1]);
    foreach ($r->getParameters() as $parameter) {
        if ('Metinet\Http\Request' === $parameter->getClass()->name) {
            $callArgs = array($request);
            break;
        }
    }
    $response = call_user_func_array($resolvedController, $callArgs);
} catch (Exception $e) {
    $errorControllerClassName = $configuration->get('errorController');
    $errorController = new $errorControllerClassName($viewRenderer);
    $errorController->setException($e);
    $resolvedController = [$errorController, $configuration->get('errorAction')];
    $response = call_user_func_array($resolvedController, $callArgs);
}

/** @var Response $response */

$response->send();
